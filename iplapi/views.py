from django.shortcuts import render
from .models import Matches, Deliveries
from .serializers import MatchesSerializer, DeliveriesSerializer
from rest_framework import viewsets 


# Create your views here.
class MatchViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows matches to be viewed or edited"
    """
    queryset = Matches.objects.all()
    serializer_class = MatchesSerializer


class DeliveriesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows matches to be viewed or edited"
    """
    queryset = Deliveries.objects.all()
    serializer_class = DeliveriesSerializer