from django.urls import path, include
from iplapi import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'matches', views.MatchViewSet)
router.register(r'deliveries', views.DeliveriesViewSet)

urlpatterns = [
    path('', include(router.urls)),
]